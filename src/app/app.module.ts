import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


import { AppComponent } from './app.component';
// import { TwoAComponent } from './portal/tutorials/two-a/two-a.component';
// import { TutorialsComponent } from './portal/tutorials/tutorials.component';
// import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { AppRoutingModule } from './app-routing.module';
import { PortalComponent } from './portal/portal.component';
import { AppsComponent } from './portal/apps/apps.component';
import { SourceComponent } from './portal/source/source.component';
import { AboutComponent } from './portal/about/about.component';
import { FooterComponent } from './footer/footer.component';

// const appRoutes: Routes =[
//   {path: 'tutorials/2a', component: TwoAComponent},
//   {path: '', component: AppComponent},
//   {path: 'tutorials', component: TutorialsComponent},
//   {path: '**', component: PagenotfoundComponent}
// ]; this whole thing is moved to app-routing module

@NgModule({
  declarations: [
    AppComponent,
    PortalComponent,
    AppsComponent,
    SourceComponent,
    AboutComponent,
    FooterComponent,
    // TwoAComponent,
    // TutorialsComponent,
    // PagenotfoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
    // RouterModule.forRoot(appRoutes) //routermodule must be imported because of reasons
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
