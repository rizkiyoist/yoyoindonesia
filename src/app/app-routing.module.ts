import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common'; this is unused, delete after generating this module
//this file must first be generated
//it is best to configure the router separately like this
import {RouterModule, Routes} from '@angular/router'; //import this to get router going
import {TutorialsComponent} from './portal/tutorials/tutorials.component'; //import this to get router working to this dir
import {TwoAComponent} from './portal/tutorials/two-a/two-a.component';
//import {AppComponent} from './app.component';
import {PagenotfoundComponent} from './pagenotfound/pagenotfound.component';
import {PortalComponent} from './portal/portal.component';

const routes: Routes =[ //renamed from appRoutes to routes
  {path: 'tutorials/2a', component: TwoAComponent},
  {path: '', component: PortalComponent},
  {path: 'tutorials', component: TutorialsComponent},
  {path: '**', component: PagenotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], //The method is called forRoot() because you configure the router at the application's root level. The forRoot() method supplies the service providers and directives needed for routing, and performs the initial navigation based on the current browser URL.
  exports: [RouterModule],

  declarations: [ //source of problem
  TwoAComponent,
  TutorialsComponent,
  PagenotfoundComponent
  ]
})
 
export class AppRoutingModule { }