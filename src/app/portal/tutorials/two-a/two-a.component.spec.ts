import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoAComponent } from './two-a.component';

describe('TwoAComponent', () => {
  let component: TwoAComponent;
  let fixture: ComponentFixture<TwoAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
